#!/usr/bin/python
# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Wrapper around the various portage programs.

This enables the portage tools to be relocatable.  The wrapper will dynamically
detect the right paths and then execute the portage programs.
"""

from __future__ import print_function

import getpass
import grp
import multiprocessing
import os
import sys


base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
portage_path = os.path.join(base_path, 'lib', 'portage')
sys.path.insert(0, os.path.join(portage_path, 'pym'))
os.environ['PYTHONPATH'] = ':'.join(sys.path)
os.environ['EPREFIX'] = '/'

os.environ.update({
    'MAKEOPTS': '-j%i' % multiprocessing.cpu_count(),
    'PORTAGE_USER': getpass.getuser(),
    'PORTAGE_GROUP': grp.getgrgid(os.getgid()).gr_name,
    'PORTAGE_ROOT_USER': getpass.getuser(),
    'PORTAGE_INST_UID': str(os.getuid()),
    'PORTAGE_INST_GID': str(os.getgid()),
})

os.execv(os.path.join(portage_path, 'bin', os.path.basename(__file__)), sys.argv)
